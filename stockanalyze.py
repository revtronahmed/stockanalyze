blurb = '''Stockanalyze v1.0'''
copyrightnotice = '''Copyright © 2021 Owais Shaikh'''

try:
    import subprocess, time, os, datetime, math, sys
    from datetime import date
    from time import strptime
    import json
    from gnews import GNews
    import yfinance as yf
    from goose3 import Goose
    import termplot
    import requests
    from polyglot.text import Text
    import tk
    from polyglot.downloader import downloader
    downloader.download("sentiment2.en", quiet=True)
    downloader.download("embeddings2.en", quiet=True)
    downloader.download("ner2.en", quiet=True)

except ImportError:
    import traceback
    traceback.print_exc()
    print("\nInstalling missing libraries. This can take some time...")
    time.sleep(3)
    subprocess.call(['pip', 'cache', 'purge'])
    subprocess.call(['pip', 'uninstall', '-r', 'requirements.txt'])
    subprocess.call(['pip', 'install', '-r', 'requirements.txt'])
    url = 'git+https://github.com/aboSamoor/polyglot.git@master'
    subprocess.call(['pip3', 'install', '-U', ''])
    time.sleep(1)
    print("\nDone! Restart program... :)")
    sys.exit()

def clear():
    _ = subprocess.call('clear' if os.name =='posix' else 'cls')

def getCompanyName (company_code):
    name = yf.Ticker(company_code)
    company_name = name.info['shortName']
    return company_name

def getCompanyCode (company_name):
    url = "https://s.yimg.com/aq/autoc"
    parameters = {'query': company_name, 'lang': 'en-US'}
    response = requests.get(url = url, params = parameters)
    data = response.json()
    company_code = data['ResultSet']['Result'][0]['symbol']
    return company_code

def getNewsArticles (company_info, time_period, results):
    news_urls = {}
    google_news = GNews(language='en', period=time_period, max_results=results)
    response = google_news.get_news(company_info)
    for article in response:
        url = article.get('url')
        date = article.get('published date')
        day = date.split(' ')[1]
        month = strptime(date.split(' ')[2],'%b').tm_mon
        year = date.split(' ')[3]
        datestamp = str(year) + str(month) + str(day) 
        news_urls[datestamp] = url

    sorted_news_urls = sorted(news_urls.items())
    return dict(sorted_news_urls).values()

def extractNewsData (url):
    try:
        response = requests.get(url)
        extractor = Goose()
        article = extractor.extract(raw_html=response.content)
        text = article.cleaned_text
        return text
    except:
        pass

def analyzeSentiment (paragraph):
    try:
        processed_text = Text(paragraph)
        score = processed_text.polarity
        score = score + 1.0 
        score = (score/2) * 100
        return int(score)
    except:
        return 0 #incompatible, unreachable or corrupt articles

def plotGraph (xaxis):
    for b in range (0, len(xaxis)): # spread graph out
        xaxis.insert (b * 2, 0)
    
    termplot.plot (xaxis)

def reputationInEnglish (avg):
    if avg >= 0 and avg <= 25: return "terrible"
    if avg >= 25 and avg <= 50: return "mediocre"
    if avg >= 50 and avg <= 75: return "good"
    if avg >= 75 and avg <= 100: return "fantastic"

def getStockData (company_info, time_period):
    company = yf.Ticker(company_info)
    data = company.history(period = time_period, interval='1d')
    close_data = dict(data.Close).items()
    close_prices = list(dict(data.Close).values())
    dates = []

    for data in close_data:
        key = str(data[0]).replace("-", "").partition(" ")
        key = key[0]
        dates.append(key)

    prices = dict(zip(dates, close_prices))
    sorted_prices = sorted(prices.items())
    return dict(sorted_prices).values()

def convertPriceToPercentage (price_list, company_info):
    company = yf.Ticker(company_info)
    data = company.history(period = '365d', interval='1d')
    high_data = list(data.High)

    highest = max(high_data)
    percentaged_list = []
    for price in price_list:
        percentage = 100 - ((price / highest) * 100)
        percentaged_list.append (percentage)
    
    return percentaged_list

if __name__ == "__main__":
    clear()
    time_period='7d'
    results = 100
    today = date.today()
    print (blurb + " (" + today.strftime("%B %d, %Y")  + ")\n" + copyrightnotice + "\n")
    if len(sys.argv) == 1:
        company = input ("Enter company name / symbol (Eg. GameStop or GME) ▶ ")
    else:
        company = sys.argv[1]

    company = getCompanyCode(company)

    print ('Getting news articles about '+ getCompanyName(company) + ' (' + company + ')\n')
    article_urls = getNewsArticles(company, time_period, results)

    print ('Getting news articles ')

    sentiment_xaxis = []

    index = 0
    for url in article_urls:
        index +=1
        print ('Analyzing market sentiment (' + str(index) + ' / '+ str(len(article_urls)) + ' articles)')
        text = extractNewsData(url)
        sentiment_xaxis.append (analyzeSentiment (text))

    print('\nGetting stock close prices\n')
    prices = getStockData (company, time_period)

    avg = sum(sentiment_xaxis)/len(sentiment_xaxis)
    print("\nReputation graph:\n")
    plotGraph (sentiment_xaxis)

    print('\n--------------------------\n')
    print("Stock graph:\n")
    plotGraph (convertPriceToPercentage (prices, company))
    print ("\nFor the past " + time_period + ", " + getCompanyName(company) + " had a " + reputationInEnglish(avg) + " reputation (" + str(int(avg)) +"%).")
    